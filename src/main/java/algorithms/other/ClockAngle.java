package algorithms.other;

import java.time.LocalDateTime;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/3/21 10:17 AM
 */
public class ClockAngle {


    private static Double clockAngle(LocalDateTime localDateTime) {

        Double hour = (localDateTime.getHour()) / 12.0 * 360;
        Double min = (localDateTime.getMinute() / 60.0) * 360;
        System.out.println(hour);
        System.out.println(min);
        Double result = hour > min ? hour - min : min - hour;

        return result;
    }

    public static void main(String[] args) {
        Double angle = clockAngle(LocalDateTime.now());
        System.out.println(angle);
    }

}
