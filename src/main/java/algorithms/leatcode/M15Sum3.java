package algorithms.leatcode;

import java.util.*;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/21 11:19 AM
 */
public class M15Sum3 {
    public static List<List<Integer>> threeSum(int[] nums) {
        Set<List<Integer>> tempres = new HashSet<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 2; i++) {
            int start = i + 1;
            int end = nums.length - 1;
            while (start < end && (i == 0 || nums[i] != nums[i - 1])) {
                int se = nums[start] + nums[end];
                int sum = nums[i] + se;
                if (sum == 0) {
                    List<Integer> temp = new ArrayList<>();
                    temp.add(nums[i]);
                    temp.add(nums[start]);
                    temp.add(nums[end]);
                    tempres.add(temp);
                    start++;
                    end--;
                } else if (sum > 0) {
                    end--;
                } else if (sum < 0) {
                    start++;
                }
            }
        }
        return new ArrayList<>(tempres);
    }

    public static List<List<Integer>> threeSumFast(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums.length < 3) {
            return res;
        }
        int max = nums[0], min = nums[0], positiveCount = 0, negativeCount = 0, zeroCount = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                max = nums[i];
            }
            if (nums[i] < min) {
                min = nums[i];
            }
            if (nums[i] > 0) {
                positiveCount++;

            } else if (nums[i] < 0) {
                negativeCount++;
            } else {
                zeroCount++;
            }
        }
        if (zeroCount >= 3) {
            res.add(Arrays.asList(0, 0, 0));
        }
        if (positiveCount == 0 || negativeCount == 0) {
            return res;
        }
        //说明对max不存在两项使得总和为0
        if (min * 2 + max > 0) {
            max = -2 * min;
            //说明对min不存在两项使得总和为0
        } else if (max * 2 + min < 0) {
            min = -2 * max;
        }
        //存放max与min之间的数的个数
        int[] map = new int[max - min + 1];
        int[] positive = new int[positiveCount];
        int[] negative = new int[negativeCount];
        positiveCount = 0;
        negativeCount = 0;
        for (int v : nums) {
            if (v >= min && v <= max) {
                if (map[v - min]++ == 0) {
                    if (v > 0) {
                        positive[positiveCount] = v;
                        positiveCount++;
                    }
                    if (v < 0) {
                        negative[negativeCount] = v;
                        negativeCount++;
                    }
                }
            }
        }
        Arrays.sort(positive, 0, positiveCount);
        Arrays.sort(negative, 0, negativeCount);
        int basej = 0;
        for (int i = negative.length - 1; i >= 0; i--) {
            int nv = negative[i];
            int minp = -nv / 2;
            while (basej < positive.length && positive[basej] < minp) {
                basej++;
            }
            for (int j = basej; j < positive.length; j++) {
                //2*pv>-nv
                int pv = positive[j];
                //cv=-nv-pv>2*pv-pv=pv
                int cv = 0 - nv - pv;
                if (cv >= nv && cv <= pv) {
                    if (cv == nv) {
                        if (map[nv - min] > 1) {
                            res.add(Arrays.asList(nv, nv, pv));
                        }
                    } else if (cv == pv) {
                        if (map[pv - min] > 1) {
                            res.add(Arrays.asList(nv, pv, pv));
                        }
                    } else {
                        if (map[cv - min] > 0) {
                            res.add(Arrays.asList(nv, cv, pv));
                        }
                    }
                } else if (cv < nv) {
                    break;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{-4, -2, 1, -5, -4, -4, 4, -2, 0, 4, 0, -2, 3, 1, -5, 0}));
    }

}