package algorithms.leatcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/21 10:29 AM
 */
public class E13RomanToInteger {

    public int romanToInt(String s) {
        int result = 0;
        Map<Character, Integer> m = new HashMap(16) {{
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('L', 50);
            put('C', 100);
            put('D', 500);
            put('M', 1000);
        }};
        char pC = '\u0000';
        for (char c : s.toCharArray()) {
            result += m.get(c);
            if (m.getOrDefault(pC, 4000) < m.get(c)){
                result = result - (2 * m.get(pC));
            }
            pC = c;
        }
        return result;
    }

    public static void main(String[] args) {

    }
}
