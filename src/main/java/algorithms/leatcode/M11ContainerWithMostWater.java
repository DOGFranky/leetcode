package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/20 6:45 PM
 */
public class M11ContainerWithMostWater {

    public int maxAreaFastest(int[] height) {
        int left = 0;
        int right = height.length - 1;
        int max = 0;
        while (left < right) {
            int lh = height[left];
            int rh = height[right];
            int area = (right - left) * (lh > rh ? rh : lh);
            max = area > max ? area : max;
            if (height[left] <= height[right]) {
                while (left < right && height[left] <= lh) {
                    left++;
                }
            } else {
                while (left < right && height[right] <= rh) {
                    right--;
                }
            }

        }
        return max;
    }

    public static int maxArea(int[] height) {
        int result = 0, temp;
        for (int i = 0; i < height.length; i++) {
            for (int j = i; j < height.length; j++) {
                temp = (height[i] > height[j] ? height[j] : height[i]) * (i > j ? i - j : j - i);
                if (temp > result) {
                    result = temp;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {

        System.out.println(maxArea(new int[]{1, 1}));
        System.out.println(maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}));


    }
}
