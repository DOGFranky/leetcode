package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/13 3:34 PM
 */
public class E7ReverseInteger {

    public static int reverseFastest(int x) {
        long res = 0;
        while (x != 0) {
            res = res * 10 + x % 10;
            x = x / 10;
        }
        return (int)res == res ? (int)res : 0;
    }

    public static Integer reverse(int x) {
        if (x==0){
            return 0;
        }
        Boolean positive = x > 0;
        StringBuffer sb = new StringBuffer(String.valueOf(x));
        if (!positive) {
            sb.delete(0,1);
        }
        Long result = Long.valueOf(sb.reverse().toString());
        if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE) {
            return 0;
        }
        if (!positive) {
            result = result * -1;
        }
        return result.intValue();
    }

    public static void main(String[] args) {
        System.out.println(reverseFastest(-2147483648));
    }
}
