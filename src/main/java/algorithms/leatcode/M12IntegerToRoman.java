package algorithms.leatcode;

public class M12IntegerToRoman {
    public static String intToRoman(int num) {
        char[] array = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        String s = "";

        while (num > 0) {
            int e = (int) Math.log10(num);
            int index = 2 * e;
            int p = num / (int) Math.pow(10, e);
            num = num % (int) Math.pow(10, e);
            while (p > 0) {
                if (p == 9) {
                    s += array[index] + "" + array[index + 2];
                    p -= 9;
                } else if (p >= 5) {
                    s += array[index + 1];
                    p -= 5;
                } else if (p == 4) {
                    s += array[index] + "" + array[index + 1];
                    p -= 4;
                } else {
                    s += array[index];
                    p--;
                }
            }
        }
        return s;
    }


    public static void main(String[] args) {
        System.out.println(intToRoman(1994));
    }
}