package algorithms.leatcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/1/4 10:18 AM
 */
public class E1TwoSum {
    /**
     * Fastest
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSumFastest(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap();

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                return new int[]{map.get(nums[i]), i};
            }
            map.put(target - nums[i], i);
        }
        return null;
    }
    /**
     * Self
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    result[0] = i;
                    result[1] = j;
                    return result;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{3, 2, 4};
        int target = 6;

        System.out.println(Arrays.toString(twoSum(nums, target)));

    }
}
