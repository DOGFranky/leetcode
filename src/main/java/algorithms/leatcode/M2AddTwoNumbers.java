package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/1/4 10:35 AM
 */
public class M2AddTwoNumbers {
    static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    /**
     * Fastest
     * @param l1
     * @param l2
     * @return
     */
    public static ListNode addTwoNumbersFastest(ListNode l1, ListNode l2) {
        ListNode top = new ListNode(-1);
        ListNode l3 = new ListNode(-1);
        top.next = l3;
        int carry = 0;
        while (l1 != null || l2 != null){
            l3.next = new ListNode(0);
            l3 = l3.next;
            int a= 0;
            int b = 0;
            if (l1 == null){
                a = 0;
            } else {
                a = l1.val;
                l1 = l1.next;
            }
            if (l2 == null){
                b = 0;
            } else {
                b = l2.val;
                l2 = l2.next;
            }
            int result = (carry + a + b)%10;
            carry = (carry + a + b)/10;

            l3.val = result;
        }
        if (carry == 1){
            l3.next = new ListNode(1);
        }
        return top.next.next;
    }

    /**
     * Self
     * @param l1
     * @param l2
     * @return
     */
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null || l2 == null) {
            return null;
        }
        int sum = l1.val + l2.val;
        int carry = sum / 10;
        l1 = l1.next;
        l2 = l2.next;
        ListNode l3 = new ListNode(sum % 10);
        ListNode temp = l3;
        while (l1 != null || l2 != null || carry != 0) {
            sum = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + carry;
            temp.next = new ListNode(sum % 10);
            carry = sum / 10;
            temp = temp.next;
            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        return l3;
    }


    public static void main(String[] args) {
//        Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
//[1,0,9]
//[5,7,8]
//[6,7,7,1]
//        [9]
        ListNode l1 = new ListNode(9);
//        l1.next = new ListNode(0);
//        l1.next.next = new ListNode(9);

        ListNode l2 = new ListNode(1);
        l2.next = new ListNode(9);
        l2.next.next = new ListNode(9);
        l2.next.next.next = new ListNode(9);
        l2.next.next.next.next = new ListNode(9);
        l2.next.next.next.next.next = new ListNode(9);
        l2.next.next.next.next.next.next = new ListNode(9);
        l2.next.next.next.next.next.next.next = new ListNode(9);
        l2.next.next.next.next.next.next.next.next = new ListNode(9);
        l2.next.next.next.next.next.next.next.next.next = new ListNode(9);

        ListNode l3 = addTwoNumbers(l1, l2);
        do {
            System.out.print(l3.val);
        } while ((l3 = l3.next) != null);

        System.out.println();
    }

}
