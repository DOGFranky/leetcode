package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/1/28 2:10 PM
 */
public class M6ZigZagConversion {


    public static String convert(String s, int numRows) {
        int r = 0;
        int diagonals = numRows - 2;
        if (s.equals("\\s+") || diagonals < 0) {
            return s;
        }
        StringBuffer sb = new StringBuffer();

        while (r < numRows) {
            for (int i = 0; i < s.length(); i++) {
                if (i % (numRows + diagonals) == r || i % (numRows + diagonals) == (numRows + diagonals - r)) {
                    sb.append(s.charAt(i));
                }
            }
            r++;
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(convert("PAYPALISHIRING", 3));
        System.out.println(convert("PAYPALISHIRING", 4));
    }

}
