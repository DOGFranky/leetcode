package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/20 4:28 PM
 */
public class H10RegularExpressionMatching {
    public boolean isMatch(String s, String p) {
        char[] sc = s.toCharArray();
        char[] pc = p.toCharArray();
        return isMatch(sc, pc, 0, 0);
    }

    private boolean isMatch(char[] sc, char[] pc, int i, int j) {
        if (j == pc.length){
            return i == sc.length;
        }
        if (j < pc.length - 1) {
            if (pc[j + 1] != '*') {
                if (i < sc.length && (pc[j] == '.' || pc[j] == sc[i])) {
                    return isMatch(sc, pc, i + 1, j + 1);
                } else {
                    return false;
                }
            } else {
                if (i < sc.length && (pc[j] == '.' || pc[j] == sc[i])) {
                    return isMatch(sc, pc, i + 1, j) || isMatch(sc, pc, i, j + 2);
                } else {
                    return isMatch(sc, pc, i, j + 2);
                }
            }
        }
        return i < sc.length && (pc[j] == '.' || sc[i] == pc[j]) && isMatch(sc, pc, i + 1, j + 1);
    }
    public static void main(String[] args) {

    }
}