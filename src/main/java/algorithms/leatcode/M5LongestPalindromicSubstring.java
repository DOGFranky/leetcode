package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/1/21 3:11 PM
 */
public class M5LongestPalindromicSubstring {

    public static String longestPalindrome(String s) {
        int len = s.length();
        boolean[][] dp = new boolean[len + 1][len + 1];
        for (int i = 0; i <= len; i++) {
            dp[i][i] = true;
        }
        int max = 0, start = 0, end = 0;
        for (int i = len; i >= 1; i--) {
            for (int j = i; j <= len; j++) {
                if (i == j) {
                    dp[i][j] = true;
                } else if (s.charAt(i - 1) == s.charAt(j - 1)) {
                    dp[i][j] = dp[i + 1][j - 1] || i + 1 == j;
                }
                if (dp[i][j] && j - i + 1 > max) {
                    max = j - i + 1;
                    start = i - 1;
                    end = j;
                }
            }
        }
        return s.substring(start, end);
    }

    public static void main(String[] args) {
        System.out.println(longestPalindrome("acfascsa"));
    }
}
