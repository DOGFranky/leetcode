package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/20 4:22 PM
 */
public class E9PalindromeNumber {
    public static boolean isPalindrome(int x) {
        char[] array = Integer.toString(x).toCharArray();
        int length = array.length - 1;
        for (int i = 0; i <= length / 2; i++) {
            if (array[i] != array[length - i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(1121));
        System.out.println(isPalindrome(11211));
        System.out.println(isPalindrome(1441211));
    }

}
