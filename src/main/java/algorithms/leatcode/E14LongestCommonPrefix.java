package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/21 10:49 AM
 */
public class E14LongestCommonPrefix {
    public static String longestCommonPrefix(String[] strs) {

        if (strs.length == 0) {
            return "";
        }
        String prefix = strs[0];
        for (int i = 1; i < strs.length && !prefix.isEmpty(); i++) {
            while (!strs[i].startsWith(prefix)) {
                prefix = prefix.substring(0, prefix.length() - 1);
            }
        }
        return prefix;

    }

    public static void main(String[] args) {
        System.out.println(longestCommonPrefix(new String[]{"aflower", "aflow", "wfloight"}));
    }

}
