package algorithms.leatcode;

import java.math.BigDecimal;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/2/20 3:26 PM
 */
public class M8StringToInteger {

    public static int myAtoi(String str) {
        if (str == null || str.trim().length() == 0) {
            return 0;
        }

        str = str.trim();
        int sign = str.charAt(0) == '-' ? -1 : 1;
        int result = 0;
        if (str.charAt(0) == '+' || str.charAt(0) == '-') {
            str = str.substring(1);
        }

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!Character.isDigit(c)) {
                break;
            }
            int x = c - '0';
            if ((Integer.MAX_VALUE - x) / 10 < result) {
                return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
            result = (result * 10) + x;
        }

        return result * sign;
    }

    public static void main(String[] args) {
//        System.out.println(myAtoi("-"));
//        System.out.println(myAtoi("   -42"));
//        System.out.println(myAtoi("4193 with words"));
//        System.out.println(myAtoi("words and 987"));
//        System.out.println(myAtoi("-91283472332"));
        System.out.println(myAtoi("20000000000000000000"));
    }
}
