package algorithms.leatcode;

import java.util.HashSet;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/1/4 1:44 PM
 */
public class M3LongestSubstringWithoutRepeatingCharacters {

    public int lengthOfLongestSubstringFastest(String s) {
        int n = s.length(), ans = 0;
        int[] index = new int[128];
        for (int j = 0, i = 0; j < n; j++) {
            i = Math.max(index[s.charAt(j)], i);
            ans = Math.max(ans, j - i + 1);
            index[s.charAt(j)] = j + 1;
        }
        return ans;
    }

    public static int lengthOfLongestSubstring(String s) {

        char[] chars = s.toCharArray();

        HashSet existed;
        int maxCount = 0, count;

        for (int i = 0; i < chars.length; i++) {
            existed = new HashSet();
            count = 0;
            for (int j = i; j < chars.length; j++) {
                if (existed.contains(chars[j])) {
                    break;
                }
                count++;
                existed.add(chars[j]);
            }
            if (count > maxCount) {
                maxCount = count;
            }
        }

        return maxCount;
    }

    public static void main(String[] args) {
        String str1 = "abcabcbb";
        System.out.println(lengthOfLongestSubstring(str1));
        String str2 = "bbbbb";
        System.out.println(lengthOfLongestSubstring(str2));
        String str3 = "pwwkew";
        System.out.println(lengthOfLongestSubstring(str3));
        String str4 = "dvdf";
        System.out.println(lengthOfLongestSubstring(str4));
    }
}
