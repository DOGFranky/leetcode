package algorithms.leatcode;

/**
 * @author jiutou
 * @version 1.0
 * @Date 2019/1/4 2:46 PM
 */
public class H4MedianofTwoSortedArrays {

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int total = nums1.length + nums2.length;
        int[] s = new int[total / 2 + 1];
        int i = 0, j = 0, k = 0;
        while (k < s.length) {
            if (i < nums1.length && j < nums2.length) {
                s[k++] = (nums1[i] <= nums2[j]) ? nums1[i++] : nums2[j++];
            } else if (i < nums1.length && k < s.length && j >= nums2.length) {
                s[k++] = nums1[i++];
            } else if (i >= nums1.length && k < s.length && j < nums2.length) {
                s[k++] = nums2[j++];
            }
        }
        return (total % 2 == 0) ? ((s[s.length - 1] + s[s.length - 2]) / 2.0) : s[s.length - 1];
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{1, 6, 7, 8, 9, 10, 3};
        int[] nums2 = new int[]{2, 5, 1, 4, 55, 12, 62, 45};
        System.out.println(findMedianSortedArrays(nums1, nums2));
    }
}
